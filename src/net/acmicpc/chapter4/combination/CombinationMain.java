package net.acmicpc.chapter4.combination;

public class CombinationMain {
    public static void main(String[] args) {
        int testN = 25;
        int testM = 12;

        Combination combination = new Combination();

        System.out.println(combination.combination(testN, testM));
        System.out.println(combination.tenCountOfCombination(testN, testM));
    }


}
