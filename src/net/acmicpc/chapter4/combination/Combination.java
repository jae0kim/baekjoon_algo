package net.acmicpc.chapter4.combination;

import net.acmicpc.chapter4.factorial.Factorial;

public class Combination {
    public long combination(int n, int m) {
        Factorial factorial = new Factorial();
        long nFactorial = factorial.simpleFactorial(n);
        long mFactorial = factorial.simpleFactorial(m);
        long nmFactorial = factorial.simpleFactorial(n-m);

        long result = nFactorial / (mFactorial * nmFactorial);

        return result;
    }

    public int tenCountOfCombination(int n, int m) {
        int countOfTwo = 0;
        int countOfFive = 0;

        for (int i=2; i<=n; i*=2) {
            countOfTwo += n/i;
        }

        for (int i=2; i<=m; i*=2) {
            countOfTwo -= m/i;
        }

        for (int i=2; i<=(n-m); i*=2) {
            countOfTwo -= (n-m)/i;
        }


        for (int i=5; i<=n; i+=5) {
            countOfFive += n/i;
        }
        for (int i=5; i<=m; i+=5) {
            countOfFive -= m/i;
        }
        for (int i=5; i<=(n-m); i+=5) {
            countOfFive -= (n-m)/i;
        }

        return Math.min(countOfTwo, countOfFive);
    }
}
