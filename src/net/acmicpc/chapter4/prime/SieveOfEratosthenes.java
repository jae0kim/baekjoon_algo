package net.acmicpc.chapter4.prime;

public class SieveOfEratosthenes {
    private int startNumber = 2;
    private int endNumber;
    private int primeNumberCount = 0;
    private int[] primeArray;
    private boolean[] isRemovedArray;
    private PrimeNumber primeNumber;

    public SieveOfEratosthenes(int endNumber) throws Exception {
        if (endNumber < this.startNumber) throw new Exception();
        this.endNumber = endNumber;
    }

    public SieveOfEratosthenes(int startNumber, int endNumber) throws Exception {
        if (endNumber < startNumber) throw new Exception();
        this.startNumber = startNumber;
        this.endNumber = endNumber;
    }

    public int[] getPrimeNumberArray() {
        this.primeArray = new int[this.endNumber - this.startNumber];
        this.isRemovedArray = new boolean[this.endNumber+1];
        this.primeNumber = new PrimeNumber();

        for (int i = this.startNumber; i<=this.endNumber; i++) {

            if (isRemovedArray[i] == false && primeNumber.isPrimeNumber(i)) {
                this.primeArray[primeNumberCount++] = i;
                for (int j=i*i; j<=this.endNumber; j+=i) {
                    this.isRemovedArray[j] = true;
                }
            }
        }

        int[] resultArray = new int[this.primeNumberCount];
        System.arraycopy(this.primeArray, 0, resultArray, 0, primeNumberCount);

        return resultArray;
    }
}
