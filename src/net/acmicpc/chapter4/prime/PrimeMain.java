package net.acmicpc.chapter4.prime;

public class PrimeMain {
    public static void main(String[] args) throws Exception {
//        int testNumber = 10;
//        PrimeNumber primeNumber = new PrimeNumber();
//        System.out.println(primeNumber.isPrimeNumber(testNumber));


        SieveOfEratosthenes sieve = new SieveOfEratosthenes(4, 200);

        int[] result = sieve.getPrimeNumberArray();

        for (int n : result) {
            System.out.println(n);
        }
    }
}
