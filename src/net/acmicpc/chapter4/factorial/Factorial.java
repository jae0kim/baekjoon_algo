package net.acmicpc.chapter4.factorial;

public class Factorial {

    public long simpleFactorial(int number) {
        long result = 1;
        while (number > 0) {
            result = number * result;
            number--;
        }

        return result;
    }

    public int tenCountOfFactorial(int number) {
        int ans = 0;

        for (int i=5; i<=number; i*=5) {
            ans += number/i;
        }
        return ans;
    }
}
