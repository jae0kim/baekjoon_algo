package net.acmicpc.chapter4.factorial;

public class FactorialMain {
    public static void main(String[] args) {

        int testNumber = 10;
        Factorial factorial = new Factorial();

        System.out.println(factorial.simpleFactorial(testNumber));
        System.out.println(factorial.tenCountOfFactorial(testNumber));
    }




}
