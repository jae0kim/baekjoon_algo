package net.acmicpc.chapter4.gcdlcm;

import java.util.Scanner;

public class ModularArithmetic {
    public static void main(String[] args) {
        // (A+B)%C
        // (A%C + B%C)%C
        // (AxB)%C
        // (A%C x B%C)%C

        Scanner scanner = new Scanner(System.in);
        String abc = scanner.nextLine();

        String[] abcArr = abc.split("\\s");
        int a = Integer.parseInt(abcArr[0]);
        int b = Integer.parseInt(abcArr[1]);
        int c = Integer.parseInt(abcArr[2]);

        int ans1 = (a+b)%c;
        int ans2 = (a%c + b%c)%c;
        int ans3 = (a*b)%c;
        int ans4 = ((a%c)*(b%c))%c;

        System.out.println(ans1);
        System.out.println(ans2);
        System.out.println(ans3);
        System.out.println(ans4);

    }
}
