package net.acmicpc.chapter4.gcdlcm;

public class GreatestCommonDivisor {

    public int getGcd(int a, int b) {

        int gcd = 1;
        int range = Math.min(a, b);

        for (int i=2; i<=range; i++) {
            if (a%i == 0 && b%i == 0) {
                gcd = i;
            }
        }


        return gcd;
    }


    public int euclidAlgo(int a, int b) {

        int r = a%b;
        if (r != 0) {
            return euclidAlgo(b, r);
        } else {
            return b;
        }
    }

    public int euclidNotUsingRecursive(int a, int b) {
        while (b != 0) {
            int r = a%b;
            a = b;
            b = r;
        }

        return a;
    }

}
