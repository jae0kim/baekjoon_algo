package net.acmicpc.chapter4.gcdlcm;

import java.util.Scanner;

public class ExamGCDLCM {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String ab = scanner.nextLine();
        String[] abArr = ab.split("\\s");
        int a = Integer.parseInt(abArr[0]);
        int b = Integer.parseInt(abArr[1]);

        GreatestCommonDivisor gcm = new GreatestCommonDivisor();

//        System.out.printf("(general)두 수 %d, %d의 GCD 는 %d \r\n", a, b, gcm.getGcd(a, b));
//        System.out.printf("(eculid)두 수 %d, %d의 GCD 는 %d \r\n", a, b, gcm.euclidAlgo(a, b));
//        System.out.printf("(eculid recurX)두 수 %d, %d의 GCD 는 %d", a, b, gcm.euclidNotUsingRecursive(a, b));

        int gcmValue = gcm.euclidAlgo(a, b);

        System.out.println(gcmValue);           // gcd
        System.out.println((a*b)/gcmValue);     // lcm

    }
}
