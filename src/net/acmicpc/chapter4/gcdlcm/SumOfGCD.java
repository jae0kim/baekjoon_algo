package net.acmicpc.chapter4.gcdlcm;

import java.util.Scanner;

public class SumOfGCD {

    private static GreatestCommonDivisor gcd = new GreatestCommonDivisor();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCaseCount = scanner.nextInt();

        for (int i=0; i<testCaseCount; i++) {
            int valueCount = scanner.nextInt();
            int[] valueArr = new int[valueCount];
            for (int n=0; n<valueCount; n++) {
                valueArr[n] = scanner.nextInt();
            }

            sumOfGCD(valueArr);
        }

    }

    private static void sumOfGCD(int[] testCase) {

        int sum = 0;

        for (int i=0; i<testCase.length-1; i++) {
            for (int j=i+1; j<testCase.length; j++) {
                sum += gcd.getGcd(testCase[i], testCase[j]);
                System.out.printf("[%d, %d] => %d \r\n", testCase[i], testCase[j], gcd.getGcd(testCase[i], testCase[j]));

            }
        }

        System.out.println(sum);
    }

}
