package net.acmicpc.chapter4.conversion;

public class BaseConversion {

    /**
     * 10진수 -> N(scale)진수
     * @param scale
     * @param source
     * @return
     */
    public int convertDecToNScale(int scale, int source) {

        StringBuilder convertedScale = new StringBuilder();

        while (source > 0) {
            int r = source % scale;
            source = source / scale;
            convertedScale.append(r);
        }

        return Integer.valueOf(convertedScale.reverse().toString());
    }

    /**
     * N(scale) 진수 -> 10진수
     * @param scale
     * @param source
     * @return
     */
    public int convertNScaleToDec(int scale, int source) {

        int sum = 0;
        int mult = 0;
        int tempInt = (int) (Math.log10(scale)+1);
        int cipher = (int) Math.pow(10, tempInt);
        while (source != 0) {
            int number = source % cipher;       // 자릿수
            sum += number * Math.pow(scale, mult);

            source /= cipher;
            mult++;
        }

        return sum;
    }
}
