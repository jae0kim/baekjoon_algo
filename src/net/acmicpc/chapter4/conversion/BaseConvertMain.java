package net.acmicpc.chapter4.conversion;

public class BaseConvertMain {
    public static void main(String[] args) {
        BaseConversion baseConversion = new BaseConversion();

        int toDecimal = baseConversion.convertNScaleToDec(17, 216);
        System.out.println(toDecimal);

        int toNScale = baseConversion.convertDecToNScale(8, toDecimal);
        System.out.println(toNScale);
    }
}
