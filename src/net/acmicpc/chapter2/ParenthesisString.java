package net.acmicpc.chapter2;

import java.util.Scanner;
import java.util.Stack;

public class ParenthesisString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Stack<String> parenthesisStack = new Stack<>();
        boolean testFlag = false;

        String parenthesisStr = scanner.nextLine();
        for (int j=0; j<parenthesisStr.length(); j++) {
            String parenthesis = String.valueOf(parenthesisStr.charAt(j));

            if ("(".equals(parenthesis)) {
                parenthesisStack.push(parenthesis);
            } else if (")".equals(parenthesis)) {
                if (parenthesisStack.empty()) {
                    testFlag = true;
                } else {
                    parenthesisStack.pop();
                }
            } else {
                System.exit(0);
            }

        }

        if (testFlag && !parenthesisStack.empty()) {
            parenthesisStack.pop();
        }

        if (parenthesisStack.empty() && !testFlag) {
            System.out.println("YES");
        } else {
            System.out.println("NO : " + parenthesisStack);
        }
    }
}
