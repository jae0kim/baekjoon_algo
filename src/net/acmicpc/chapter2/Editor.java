package net.acmicpc.chapter2;

import java.util.*;

public class Editor {

    public static Deque<String> frontQue = new LinkedList<>();
    public static Deque<String> backQue = new LinkedList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        int textLength = text.length();
        int cursor = (int) (Math.random() * textLength) + 1;

        for (int i=0; i<textLength; i++) {
            if (i < cursor) {
                frontQue.offer(String.valueOf(text.charAt(i)));
            } else {
                backQue.offer(String.valueOf(text.charAt(i)));
            }
        }

        System.out.println("cursor : " + cursor);
        printQue();

        String polledChar;
        while (true) {
            String key = scanner.nextLine();
            // L, D, B
            if ("L".equals(key)) {
                System.out.println("pressed 'L' ");
                polledChar = frontQue.pollLast();
                backQue.addFirst(polledChar);
                printQue();
            } else if ("D".equals(key)) {
                System.out.println("pressed 'D'" );
                polledChar = backQue.pollFirst();
                frontQue.addLast(polledChar);
                printQue();
            } else if ("B".equals(key)) {
                System.out.println("pressed 'B'");
                frontQue.pollLast();
                printQue();
            } else if ("q".equals(key)) {
                break;
            } else {
                // P $
                String[] splitKey = key.split("\\s");
                if ("P".equals(splitKey[0])) {
                    frontQue.addLast(splitKey[1]);
                    printQue();
                } else {
                    break;
                }
            }

        }

    }

    public static void printQue() {
        System.out.println(frontQue);
        System.out.println(backQue);
    }
}
