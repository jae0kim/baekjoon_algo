package net.acmicpc.chapter1;

import java.util.Scanner;

public class InputOutput02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("첫번째 수를 입력하세요 : ");
        String firstNumberStr = scanner.nextLine();
        int firstNumber = Integer.parseInt(firstNumberStr);

        System.out.print("두번째 수를 입력하세요 : ");
        String secondNumberStr = scanner.nextLine();
        int secondNumber = Integer.parseInt(secondNumberStr);

        System.out.println();

        System.out.println("두 수의 합은 " +(firstNumber + secondNumber) + " 입니다");

    }
}
