package net.acmicpc.chapter1;

import java.util.Scanner;

public class Star04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int level = scanner.nextInt();

        String whiteSpace = "";
        for (int i=0; i<level; i++) {
            String star = "";
            for (int j=level-i; j>0; j--) {
                star += "*";
            }

            if (i != 0) {
                whiteSpace += " ";
            }

            System.out.println(whiteSpace+star);
        }
    }
}
