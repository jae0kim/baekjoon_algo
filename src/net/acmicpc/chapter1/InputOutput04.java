package net.acmicpc.chapter1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputOutput04 {
    public static void main(String[] args) {
        List<Integer> summaryList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        System.out.println("합할 두 수 입력");
        while (true) {
            String numbers = scanner.nextLine();
            String[] numberArr = numbers.split("\\s");
            int firstNumber = Integer.parseInt(numberArr[0]);
            int secondNumber = Integer.parseInt(numberArr[1]);
            int sum = firstNumber + secondNumber;

            if (sum == 0) {
                break;
            } else {
                summaryList.add(sum);
            }

        }

        for (int sum : summaryList) {
            System.out.println(sum);
        }
    }
}
