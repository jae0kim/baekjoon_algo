package net.acmicpc.chapter1;

import java.util.Scanner;

public class InputOutput01 {
    public static void main(String[] args) {
        System.out.print("두 수를 입력하세요 : ");

        Scanner scanner = new Scanner(System.in);

        String message = scanner.nextLine();
        String[] messageArray = message.split("\\s");

        int firstNumber = Integer.parseInt(messageArray[0]);
        int secondNumber = Integer.parseInt(messageArray[1]);

        System.out.println("두 수의 합은 " + (firstNumber+secondNumber) + " 입니다");
    }

}
