package net.acmicpc.chapter1;

import java.util.Scanner;

public class Star01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int level = scanner.nextInt();

        String star = "";
        for(int i=0; i<level; i++) {
            star += "*";
            System.out.println(star);
        }
    }
}
