package net.acmicpc.chapter1;

import java.util.Scanner;

public class InputOutput03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Test case의 갯수 : ");
        int testCaseCount = Integer.parseInt(scanner.nextLine());

        String[] testCaseArr = new String[testCaseCount];
        for (int i=0; i<testCaseCount; i++) {
            testCaseArr[i] = scanner.nextLine();
        }

        for (int j=0; j<testCaseArr.length; j++) {
            String[] numberArrStr = testCaseArr[j].split("\\s");
            int firstNumber = Integer.parseInt(numberArrStr[0]);
            int secondNumber = Integer.parseInt(numberArrStr[1]);

            System.out.println("두 수 " + firstNumber + ", " + secondNumber + " 의 합은 " + (firstNumber+secondNumber) + " 입니다");
        }
    }
}
