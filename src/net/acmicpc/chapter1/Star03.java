package net.acmicpc.chapter1;

import java.util.Scanner;

public class Star03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int level = scanner.nextInt();

        for (int i=0; i<level; i++) {
            String star = "";
            for (int j=level-i; j>0; j--) {
                star += "*";
            }

            System.out.println(star);
        }
    }
}
