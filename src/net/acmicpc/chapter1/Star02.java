package net.acmicpc.chapter1;

import java.util.Scanner;

public class Star02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int level = scanner.nextInt();

        String star = "";
        for (int i=0; i<level; i++) {

            String whiteSpace = "";
            for (int j=level-i-1; j>0; j--) {
                whiteSpace += " ";
            }

            star += "*";
            System.out.println(whiteSpace+star);
        }

    }
}
