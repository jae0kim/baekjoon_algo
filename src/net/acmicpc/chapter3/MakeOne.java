package net.acmicpc.chapter3;

public class MakeOne {

    private static int counter = 0;

    public static void main(String[] args) {

        int testNumber = 10;
        makeOne(testNumber);

        System.out.println(counter);
    }

    // 10 -> 5 -> 4 -> 2 -> 1
    public static void makeOne(int testNumber) {
        if (testNumber % 3 == 0) {
            counter++;
            makeOne(testNumber/3);
        } else if (testNumber % 2 == 0) {
            counter++;
            makeOne(testNumber/2);
        } else if (testNumber != 1) {
            counter++;
            makeOne(testNumber - 1);
        } else {
            System.out.println("Test No : " + testNumber);
        }
    }
}
