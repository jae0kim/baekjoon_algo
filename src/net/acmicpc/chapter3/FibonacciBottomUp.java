package net.acmicpc.chapter3;

public class FibonacciBottomUp {

    private static int[] data;

    public static void main(String[] args) {

        int testNumber = 10;
        data = new int[testNumber+1];

        System.out.println(doFibonacci(testNumber));
    }

    public static int doFibonacci(int n) {
        data[0] = 0;
        data[1] = 1;

        for (int i=2; i<=n; i++) {
            data[i] = data[i-2]+data[i-1];
        }

        return data[n];
    }
}
