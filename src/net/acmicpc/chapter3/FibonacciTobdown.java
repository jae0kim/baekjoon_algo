package net.acmicpc.chapter3;

public class FibonacciTobdown {

    private static int[] memo;

    public static void main(String[] args) {

        int testNo = 10;
        memo = new int[testNo+1];

        System.out.println(doFibonacci(10));
    }

    public static int doFibonacci(int n) {

        if (n <= 1) {
            return n;
        } else {
            if (memo[n] > 0) {
                return memo[n];
            }
            memo[n] = doFibonacci(n -1) + doFibonacci(n - 2);
            return memo[n];
        }
    }
}
